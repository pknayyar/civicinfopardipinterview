package com.google.civicinfo.api.cases;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.not;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.google.civicinfo.api.util.ConfigReader;
import com.google.civicinfo.api.util.Payload;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

public class ElectionQueryTest extends BaseTest {

	private static Logger logger = Logger.getLogger(ElectionQueryTest.class);
    private static final String ENDPOINT = ConfigReader.getEndPoint();
    private static final String KEY = ConfigReader.getKey();
    private static final long TIMEOUT = ConfigReader.getTimeout();

    @BeforeClass
    public void setUp() {
        RestAssured.baseURI = ConfigReader.getBaseUri();
    }
    
    /*  Test_Case_1
     * Validate the election query api to returns valid json results when API key is set
     */
    @Test
    public void verifyElectionQueryReturnsValidResults() {
    	given()
        .spec(getSpecification(ConfigReader.getApiKey()))        
      .when()
        .log()
        .all()
        .get(ENDPOINT)
      .then()
        .time(lessThan(TIMEOUT))
        .and()
        .statusCode(200)
        .body("kind", equalTo(Payload.KIND)) 
        .body("elections", is(not(empty())))
        .body("elections.size()", equalTo(3));
    }
    
    /* Test_Case_2 and Test_Case_3
     * Validate the election query api to returns valid json results when multiple API keys are executed
     */
    @Test(dataProvider = "ApiKeys")
    public void verifyElectionQueryReturnsValidResultsAgainstDataProvider(String id) {
    	given()
        .spec(getSpecification(id))        
      .when()
        .log()
        .all()
        .get(ENDPOINT)
      .then()
        .time(lessThan(TIMEOUT))
        .and()
        .statusCode(200)
        .body("kind", equalTo(Payload.KIND)) 
        .body("elections", is(not(empty())))
        .body("elections.size()", equalTo(3));
    }
    
    /* Test_Case_4
     * Validate the Election Query api to returns valid json results with valid API key and multiple threads run concurrently
     */
    @Test
    public void verifyElectionQueryReturnsValidResultsAgainstMultipleThreads() {
    	int maxThreads = 5; 
    	ExecutorService executorService = Executors.newFixedThreadPool(maxThreads);

        for (int i = 0; i < maxThreads; i++) {
            executorService.execute(new Runnable() {
                @Override
                public void run(){
                    try {
                    	verifyElectionQueryReturnsValidResults();
                    } catch (Exception e) {
                    	logger.error(e);
                    }
                }
            });
        }

        // Wait for all the threads to shutdown
        try {
            executorService.awaitTermination(5L, TimeUnit.SECONDS);
        } catch (InterruptedException ie) {
        	logger.error("Shutdown interrupted. Will try to shutdown again.", ie);
            executorService.shutdownNow();
        }
    
    }
        
    /* Test_Case_5
     * Validate the Election Query API to returns valid json results against mock DB json data set
     */
    @Test
    public void verifyElectionQueryReturnsValidResultsAgainstResponseFile() {
    	given()
        .spec(getSpecification(ConfigReader.getApiKey()))        
      .when()
        .log()
        .all()
        .get(ENDPOINT)
      .then()
        .time(lessThan(TIMEOUT))
        .and()
        .statusCode(200)
        .body(matchesJsonSchemaInClasspath("api_response.json"));
    }

    /* Test_Case_6
     * Validate the Election Query API to returns valid json error results when Invalid API key is set
     */
    @Test
    public void verifyElectionQueryWithInvalidApiKey() {
    	given()
        .spec(getSpecification(ConfigReader.getInvalidApiKey()))        
      .when()
        .log()
        .all()
        .get(ENDPOINT)
      .then()
        .time(lessThan(TIMEOUT))
        .and()
        .statusCode(400)
        .body("error.message", equalTo(Payload.BAD_REQUEST))
        .body("error.errors", is(not(empty())));
    }
    
    /* Test_Case_7
     * Validate the Election Query API to returns error when No API Key is inputed
     */
    @Test
    public void verifyElectionQueryWithoutKey() {
    	given()
        .spec(getSpecification(""))        
      .when()
        .log()
        .all()
        .get(ENDPOINT)
      .then()
        .time(lessThan(TIMEOUT))
        .and()
        .statusCode(403)
        .body("error.message", equalTo(Payload.ERROR_MESSAGE))
        .body("error.errors", is(not(empty())));
    }

    private RequestSpecification getSpecification(String apiKey){
    	  RequestSpecBuilder builder = new RequestSpecBuilder();
    	  if(StringUtils.isNotEmpty(apiKey))
    		  builder.addQueryParam(KEY, apiKey); 
    	  builder.setContentType(ContentType.JSON);
    	  builder.setAccept(ContentType.JSON);
    	  return builder.build();
    }
    
    @DataProvider(name="ApiKeys")
    public Object[][] getApiKeys() {
    	Object[] apiKeys = ConfigReader.getApiKeys();
    	return new Object[][]{{apiKeys[0]},{apiKeys[1]}};
    }
}
