package com.google.civicinfo.api.util;

public class Payload {
    public static final String ERROR_MESSAGE = "Daily Limit for Unauthenticated Use Exceeded. Continued use requires signup.";
    public static final String BAD_REQUEST = "Bad Request";
    public static final String KIND = "civicinfo#electionsQueryResponse";
}
