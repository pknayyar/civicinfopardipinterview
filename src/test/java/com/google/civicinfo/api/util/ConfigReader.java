package com.google.civicinfo.api.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Properties;

public class ConfigReader {

    private static Properties properties = null;

    static {
        InputStream inputStream = ConfigReader.class.getClassLoader().getResourceAsStream("config.properties");
        properties = new Properties();
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getBaseUri() {
        return properties.getProperty("api.base.uri");
    }
    
    public static String getEndPoint() {
        return properties.getProperty("api.elections.endpoint");
    }

    public static String getApiKey() {
        return decode(properties.getProperty("api.key"));
    }
    
    public static String getKey() {
        return properties.getProperty("key");
    }
    
    public static long getTimeout() {
        return Long.parseLong(properties.getProperty("timeout"));
    }
    
    public static Object[] getApiKeys() {
    	String[] apiKeys = properties.getProperty("api.keys").split(",");
    	List<String> keysList = new ArrayList<String>();
    	for(String apiKey : apiKeys) {
    		keysList.add(decode(apiKey));
    	}
    	return keysList.toArray();
    }

    public static String getInvalidApiKey() {
        return properties.getProperty("api.key.invalid");
    }
    
    private static String decode(String encodedKey) {
    	return new String(Base64.getDecoder().decode(encodedKey));
    }  
}
